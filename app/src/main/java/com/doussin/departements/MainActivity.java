package com.doussin.departements;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.opengl.Matrix;
import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

// En fait un a créé un nouvel objet de type EFranceDepartement. Cet objet est de la forme Ain(1,
public class MainActivity extends AppCompatActivity {

    public enum EFranceDepartment {
        Ain(1, "Ain", "Rhône-Alpes", "Bourg-en-bresse"),
        Aisne(2, "Aisne", "Hauts-de-France", "Laon"),
        Allier(3, "Allier", "Auvergne", "Moulins"),
        AlpesDeHautesProvence(4, "Alpes de Hautes-Provence", "Provence-Alpes-Côte d'Azur", "Digne Les Bains"),
        HautesAlpes(5, "Hautes-Alpes", "Provence-Alpes-Côté d'Azur", "Gap"),
        AlpesMaritimes(6, "Alpes-Maritimes", "Provence-Alpes-Côté d'Azur", "Nice"),
        Ardeche(7, "Ardèche", "Rhône-Alpes", "Privas"),
        Ardennes(8, "Ardennes", "Champagne-Ardenne", "Charleville mézières"),
        Ariege(9, "Ariege", "Midi-Pyrénées", "Foix"),
        Aube(10, "Aube", "Champagne-Ardenne", "Troyes"),
        Aude(11, "Aude", "Languedoc-Roussillon", "Carcassonne"),
        Aveyron(12, "Aveyron", "Midi-Pyrénées", "Rodez"),
        BouchesDuRhone(13, "Bouches-Du-Rhône", "Provence-Alpes-Côte d'Azur", "Marseille"),
        Calvados(14, "Calvados", "Normandie", "Caen"),
        Cantal(15, "Cantal", "Auvergne", "Aurillac"),
        Charente(16, "Charente", "Poitou-Charente", "Angoulême"),
        CharenteMaritime(17, "Charente-Maritime", "Poitou-Charente", "La Rochelle"),
        Cher(18, "Cher", "Centre", "Bourges"),
        Correze(19, "Correze", "Limousin", "Tulle"),
        CorseDuSud(20, "Corse-du-Sud", "Corse", "Ajaccio"),
        HauteCorse(20, "Haute-Corse", "Corse", "Bastia"),
        CoteDOr(21, "Cöte-d'Or", "Bourgogne", "Dijon"),
        CotesDArmor(22, "Côtes d'Armor", "Bretagne", "Saint-brieuc"),
        Creuse(23, "Creuse", "Limousin", "Gueret"),
        Dordogne(24, "Dordogne", "Aquitaine", "Périgueux"),
        Doubs(25, "Doubs", "Franche-Comté", "Besançon"),
        Drome(26, "Drôme", "Rhône-Alpes", "Valence"),
        Eure(27, "Eure", "Normandie", "Evreux"),
        EureEtLoir(28, "Eure-et-Loir", "Centre", "Chartres"),
        Finistere(29, "Finistere", "Bretagne", "Quimper"),
        Gard(30, "Gard", "Languedoc-Roussillon", "Nimes"),
        HauteGaronne(31, "Haute-Garonne", "Midi-Pyrénées", "Toulouse"),
        Gers(32, "Gers", "Midi-Pyrénées", "Auch"),
        Gironde(33, "Gironde", "Aquitaine", "Bordeaux"),
        Herault(34, "Hérault", "Languedoc-Roussillon", "Montpellier"),
        IlleEtVilaine(35, "Ille-et-Vilaine", "Bretagne", "Rennes"),
        Indre(36, "Indre", "Centre", "Châteauroux"),
        IndreEtLoire(37, "Indre-et-Loire", "Centre", "Tours"),
        Isere(38, "Isère", "Rhône-Alpes", "Grenoble"),
        Jura(39, "Jura", "Franche-Compté", "Lons le Saunier"),
        Landes(40, "Landes", "Aquitaine", "Mont-de-Marsan"),
        LoirEtCher(41, "Loir-er-Cher", "Centre", "Blois"),
        Loire(42, "Loire", "Rhône-Alpes", "Saint-etienne"),
        HauteLoire(43, "Haute-Loire", "Auvergne", "Le puy-en-velay"),
        LoireAtlantique(44, "Loire-Atlantique", "Pays de la Loire", "Nantes"),
        Loiret(45, "Loiret", "Centre", "Orléans"),
        Lot(46, "Lot", "Midi-Pyrénées", "Cahors"),
        LotEtGaronne(47, "Lot-et-Garonne", "Aquitaine", "Agen"),
        Lozere(48, "Lozère", "Languedoc-Roussillon", "Mende"),
        MaineEtLoire(49, "Maine-et-Loire", "Pays de la Loire", "Angers"),
        Manche(50, "Manche", "Normandie", "Saint-lô"),
        Marne(51, "Marne", "Champagne-Ardenne", "Châlons-en-Champagne"),
        HauteMarnes(52, "Haute-Marne", "Champagne-Ardenne", "Chaumons"),
        Mayenne(53, "Mayenne", "Pays de la Loire", "Laval"),
        MeurtheEtMoselle(54, "Meurthe-et-Moselle", "Lorraine", "Nancy"),
        Meuse(55, "Meuse", "Lorraine", "Bar le duc"),
        Morbihan(56, "Morbihan", "Bretagne", "Vannes"),
        Moselle(57, "Moselle", "Lorraine", "Metz"),
        Nievre(58, "Nièvre", "Bourgogne", "Nevers"),
        Nord(59, "Nord", "Hauts-de-France", "Lille"),
        Oise(60, "Oise", "Hauts-de-France", "Beauvais"),
        Orne(61, "Orne", "Normandie", "Alençons"),
        PasDeCalais(62, "Pas-de-Calais", "Hauts-de-France", "Arras"),
        PuyDeDome(63, "Puy-de-Dôme", "Auvergne", "Clermont-Ferrand"),
        PyreneesAtlantiques(64, "Pyrénées-Atlantique", "Aquitaine", "Pau"),
        HautesPyrenees(65, "Hautes-Pyrénées", "Midi-Pyrénées", "Tarbes"),
        PyreneesOrientales(66, "Pyrénées-Orientales", "Languedoc-Roussillon", "Perpignan"),
        BasRhin(67, "Bas-Rhin", "Alsace", "Strasbourg"),
        HautRhin(68, "Haut-Rhin", "Alsace", "Colmar"),
        Rhone(69, "Rhône", "Rhône-Alpes", "Lyon"),
        HauteSaone(70, "Haute-Sâone", "Franche-Comté", "Vesoul"),
        SaoneEtLoire(71, "Sâone-et-Loire", "Bourgogne", "Macon"),
        Sarthes(72, "Sarthes", "Pays de la Loire", "Le Mans"),
        Savoie(73, "Savoie", "Rhône-Alpes", "Chambery"),
        HauteSavoie(74, "Haute-Savoie", "Rhône-Alpes", "Annecy"),
        Paris(75, "Paris", "Ile de france", "Paris"),
        SeineMaritime(76, "Seine-Maritime", "Normandie", "Rouen"),
        SeineEtMarne(77, "Seine-et-Marne", "Ile de france", "Melun"),
        Yvelines(78, "Yvelines", "Ile de france", "Versailles"),
        DeuxSevres(79, "Deux-Sèvres", "Poitou-Charente", "Niort"),
        Sommes(80, "Sommes", "Hauts-de-France", "Amiens"),
        Tarn(81, "Tarn", "Midi-Pyrénées", "Albi"),
        TarnEtGaronne(82, "Tarn-et-Garonne", "Midi-Pyrénées", "Montauban"),
        Var(83, "Var", "Provence-Alpes-Côte d'Azur", "Toulon"),
        Vaucluse(84, "Vaucluse", "Provence-Alpes-Côte d'Azur", "Avignon"),
        Vendee(85, "Vendée", "Pays de la Loire", "La Roche Sur Yon"),
        Viennes(86, "Viennes", "Limousin", "Poitiers"),
        HauteVienne(87, "Haute-Vienne", "Limousin", "Limoges"),
        Vosges(88, "Vosges", "Lorraine", "Epinal"),
        Yonne(89, "Yonne", "Bourgogne", "Auxerre"),
        TerritoireDeBelfort(90, "Territoire-de-Belfort", "Franche-Compté", "Belfort"),
        Essone(91, "Essonne", "Ile de france", "Evry"),
        HautsDeSeine(92, "Hauts-de-Seine", "Ile de france", "Nanterre"),
        SeineSaintDenis(93, "Seine-Saint-Denis", "Ile de france", "Bobigny"),
        ValDeMarne(94, "Val-de-marne", "Ile de france", "Creteil"),
        ValDOise(95, "Val-d'oise", "Ile de france", "Pontoise"),
        Guadeloupe(971, "Guadeloupe", "Outre-mer", "Basse-Terre"),
        Martinique(972, "Martinique", "Outre-mer", "Fort-de-France"),
        GuyaneFrancaise(973, "Guyane Française", "Outre-mer", "Cayenne"),
        Reunion(974, "La Réunion", "Outre-mer", "Saint-Denis"),
        Mayotte(976, "Mayotte", "Outre-mer", "Mamoudzou");


        public static final String DEPARTMENT_NUMERO = "[n_dpt]";
        public static final String DEPARTMENT_NAME = "[dpt_name]";
        public static final String AREA_NAME = "[area]";
        public static final String PREFECTURE = "[pref]";

        String _prefecture;
        Integer _department;
        String _departmentName;
        String _area;

        // Constructeur
        EFranceDepartment(Integer department, String departmentName, String area, String prefecture) {
            _prefecture = prefecture;
            _department = department;
            _departmentName = departmentName;
            _area = area;
        }

        public String getPrefecture() {
            return _prefecture;
        }

        public Integer getDepartment() {
            return _department;
        }

        public String getDepartmentName() {
            return _departmentName;
        }

        public String getArea() {
            return _area;
        }

        public static List getList(String format, Boolean sorted) {
            LinkedList<String> list = new LinkedList<>();
            for (EFranceDepartment value : EFranceDepartment.values()) {
                String elem = format;
                elem = elem.replace(DEPARTMENT_NUMERO, value.getDepartment().toString());
                elem = elem.replace(DEPARTMENT_NAME, value.getDepartmentName());
                elem = elem.replace(AREA_NAME, value.getArea());
                elem = elem.replace(PREFECTURE, value.getPrefecture());
                list.add(elem);
            }
            if (sorted) {
                Collections.sort(list);
            }
            return list;
        }
    }
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_showresults = findViewById(R.id.btn_showresults);
        final TextView txt_Results = findViewById(R.id.Result);
        Button btn_question = findViewById(R.id.btn_question);
        final TextView txt_Question = findViewById(R.id.Question);
        Button btn_mapresult = findViewById(R.id.btn_mapresult);

        final ImageView map_image = (ImageView) findViewById(R.id.map);


        // utilisation de getList (par exemple) :
        final List MyDepartement = EFranceDepartment.getList(EFranceDepartment.DEPARTMENT_NUMERO + " - " + EFranceDepartment.DEPARTMENT_NAME + " - " + EFranceDepartment.PREFECTURE + " (" + EFranceDepartment.AREA_NAME + ")", true);
        final List MyNumDepartment = EFranceDepartment.getList(EFranceDepartment.DEPARTMENT_NUMERO, true);
        final List MyNomDepartment = EFranceDepartment.getList(EFranceDepartment.DEPARTMENT_NAME, true);
        final List MyNomPrefecture = EFranceDepartment.getList(EFranceDepartment.PREFECTURE, true);
        final List mode = MyNumDepartment;
        //à partir de là on travaille avec une LISTE de phrase du format "numdep - nomdep - pref - reg" !!! C'est chiant !
        final Random rand = new Random();
        final int[] departement = {0};


        /*private static final List<EFranceDepartment> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static EFranceDepartment randomDepartment()  {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }*/

        btn_question.setOnClickListener( new View.OnClickListener(){
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v){

                departement[0] = rand.nextInt(mode.size());
                txt_Question.setText(mode.get(departement[0]).toString());
                txt_Results.setText("Résultat");
                map_image.setImageResource(R.drawable.carte_vide);
            }
        });

        btn_showresults.setOnClickListener( new View.OnClickListener(){
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v){

                txt_Results.setText(/*"La France possède " + MyDepartement.size() + " départements.\n"*/
                                /*+ MyDepartement.get(0).toString() + "\n"
                                + MyDepartement.get(MyDepartement.size()-1).toString() + "\n"*/
                        MyDepartement.get(departement[0]).toString());
            }
        });

        btn_mapresult.setOnClickListener( new View.OnClickListener(){
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v){
                map_image.setImageResource(R.drawable.carte_reponse);
            }
        });


    }

    public String getResults(){

        String results = EFranceDepartment.Ain.getDepartment() + " - "
                        + EFranceDepartment.Ain.getDepartmentName() + " : "
                        + EFranceDepartment.Ain.getPrefecture() + " ; "
                        + EFranceDepartment.Ain.getArea() + "\n";
        //String results = EFranceDepartment.getList(, false)
        return results;

    }

}

